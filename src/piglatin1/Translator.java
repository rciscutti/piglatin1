package piglatin1;

import java.util.ArrayList;

public class Translator {
	
	public static final String NIL ="nil";
	public static final StringBuilder EMPTY = new StringBuilder("");
	private String phrase;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {

		return phrase;
	}

	public String translateSingleWord() {
		
		StringBuilder phraseBuilder=new StringBuilder(phrase);
		int j=0;
		int y=0;
		
		if(phraseBuilder.compareTo(EMPTY)==0) {
			return NIL;
		}
			
			if(startWithVowel(phraseBuilder.toString())){
				
				if (phraseBuilder.toString().endsWith("y")) {
					return phraseBuilder.append("nay").toString();
					
				} else if (endWithVowel(phraseBuilder.toString())) {
					return phraseBuilder.append("yay").toString();
				
				}else if (endWithConsonant(phraseBuilder.toString())) {
					return phraseBuilder.append("ay").toString();
				}
			}else if(startWithConsonant( phraseBuilder.toString())){
					StringBuilder phraseSwapped = new StringBuilder (phraseBuilder.toString());
					int numberOfConsonant=0;
					char temp;
					 y=0;
					 
					while(isLetterConsonant(y,phraseSwapped.toString() )) {
						
						numberOfConsonant++;
						y++;
					}
					
					for(j=0; j<numberOfConsonant; j++) {
						temp = phraseSwapped.charAt(0);
						phraseSwapped.deleteCharAt(0);
						phraseSwapped.append(temp);
					}
					
					return phraseSwapped.append("ay").toString();
				
			}
			
		return "translation failed";
	
	}
	
	
	
public String translateWordsPunctuated() {
		
		
		StringBuilder phraseBuilder=new StringBuilder(phrase);
		ArrayList<String> containerOfWords = new ArrayList<>();
		ArrayList<Integer> indexSplitter = new ArrayList<>();
		ArrayList<Character> splitterType = new ArrayList<>();
		
		int letterCounter=0;
		int numberOfSeparator=countNumberOfSeparator(phraseBuilder);
		int separatorCounter=0;
		int k=0;
		int z=0;
		
		
		
		// Initialization of indexSplitter, splitterType and containerOfWords
		for(k=0; k<phraseBuilder.length(); k++) {
			if(phraseBuilder.charAt(k) == ' '|| phraseBuilder.charAt(k) == '-'|| isPunctuated(k, phraseBuilder.toString())) {
				indexSplitter.add(k);
				splitterType =(initializeSplitterType(phraseBuilder, splitterType,  k));
				
				if(isLetterVowel(k-1,phraseBuilder.toString()) || isLetterConsonant(k-1,phraseBuilder.toString())) {
					containerOfWords.add(phraseBuilder.substring(k-letterCounter, (indexSplitter.get(z)) ));
					letterCounter=0;
				}
				
				separatorCounter++;
				z++;
				
			}else if(separatorCounter ==numberOfSeparator && numberOfSeparator != 0){
				containerOfWords.add(phraseBuilder.substring((indexSplitter.get(numberOfSeparator-1)+1), phraseBuilder.length() ));
				k=phraseBuilder.length();
					
			}else{
				letterCounter++;
			}
			
		}
		
		containerOfWords=translateWords(containerOfWords);
		return translatePhrase(phraseBuilder, splitterType, numberOfSeparator, containerOfWords);
	}
	
	private boolean startWithVowel(String phrase) {
		
		return (phrase.charAt(0)==('a') || phrase.charAt(0)==('e') || phrase.charAt(0)==('i') 
					|| phrase.charAt(0)==('o') || phrase.charAt(0)==('u'));
		
	}
	
	
			
	private boolean endWithVowel(String phrase) {
		
		return (phrase.charAt(phrase.length()-1)==('a') || phrase.charAt(phrase.length()-1)==('e') || phrase.charAt(phrase.length()-1)==('i') 
				|| phrase.charAt(phrase.length()-1)==('o') || phrase.charAt(phrase.length()-1)==('u'));
		
	}
	
	
	
	private boolean isLetterConsonant(int i, String phrase) {
		 
		return (phrase.charAt(i) == 'b' || phrase.charAt(i) == 'c'|| phrase.charAt(i) == 'd'|| phrase.charAt(i) == 'f' || phrase.charAt(i) == 'g'
				|| phrase.charAt(i) == 'h'|| phrase.charAt(i) == 'k'|| phrase.charAt(i) == 'j'|| phrase.charAt(i) == 'l'|| phrase.charAt(i) == 'm'
				|| phrase.charAt(i) == 'n'|| phrase.charAt(i) == 'p'|| phrase.charAt(i) == 'q'|| phrase.charAt(i) == 'r'|| phrase.charAt(i) == 's'
				|| phrase.charAt(i) == 't'|| phrase.charAt(i) == 'v'|| phrase.charAt(i) == 'w'|| phrase.charAt(i) == 'x'|| phrase.charAt(i) == 'y'
				|| phrase.charAt(i) == 'z');
	

	}
	
	private boolean isLetterVowel(int i, String phrase) {
		 
		return (phrase.charAt(i) == 'a' || phrase.charAt(i) == 'e'|| phrase.charAt(i) == 'i'
				|| phrase.charAt(i) == 'o' || phrase.charAt(i) == 'u');
	

	}
	
	private boolean startWithConsonant(String phrase) {
		
		return (phrase.charAt(0)==('b') || phrase.charAt(0)==('c') || phrase.charAt(0)==('d') 
					|| phrase.charAt(0)==('f') || phrase.charAt(0)==('g')|| phrase.charAt(0)==('h')
					|| phrase.charAt(0)==('k')|| phrase.charAt(0)==('j')|| phrase.charAt(0)==('l')
					|| phrase.charAt(0)==('m')|| phrase.charAt(0)==('n')|| phrase.charAt(0)==('p')
					|| phrase.charAt(0)==('q')|| phrase.charAt(0)==('r')|| phrase.charAt(0)==('s')
					|| phrase.charAt(0)==('t')|| phrase.charAt(0)==('v')|| phrase.charAt(0)==('w')
					|| phrase.charAt(0)==('y')|| phrase.charAt(0)==('x')|| phrase.charAt(0)==('z'));
		
	}

	private boolean endWithConsonant(String phrase) {
	
		return (phrase.charAt(phrase.length()-1)==('b') || phrase.charAt(phrase.length()-1)==('c') || phrase.charAt(phrase.length()-1)==('d') 
				|| phrase.charAt(phrase.length()-1)==('f') || phrase.charAt(phrase.length()-1)==('g')|| phrase.charAt(phrase.length()-1)==('h')
				|| phrase.charAt(phrase.length()-1)==('k')|| phrase.charAt(phrase.length()-1)==('j')|| phrase.charAt(phrase.length()-1)==('l')
				|| phrase.charAt(phrase.length()-1)==('m')|| phrase.charAt(phrase.length()-1)==('n')|| phrase.charAt(phrase.length()-1)==('p')
				|| phrase.charAt(phrase.length()-1)==('q')|| phrase.charAt(phrase.length()-1)==('r')|| phrase.charAt(phrase.length()-1)==('s')
				|| phrase.charAt(phrase.length()-1)==('t')|| phrase.charAt(phrase.length()-1)==('v')|| phrase.charAt(phrase.length()-1)==('w')
				|| phrase.charAt(phrase.length()-1)==('y')|| phrase.charAt(phrase.length()-1)==('x')|| phrase.charAt(phrase.length()-1)==('z'));
	
	}
	
	
	
	private boolean isPunctuated(int i, String phrase) {
		return (phrase.charAt(i) == '.'|| phrase.charAt(i) == ','|| phrase.charAt(i) == ';'|| phrase.charAt(i) == ':'
				|| phrase.charAt(i) == '?'|| phrase.charAt(i) == '!'|| phrase.charAt(i) == "\u0027".charAt(0)|| phrase.charAt(i) == '(' || phrase.charAt(i) == ')');
	}
	private int countNumberOfSeparator(StringBuilder phraseBuilder) {
		int h=0;
		int numberOfSeparator=0;
		for(h=0; h<phraseBuilder.length(); h++) {
			if(phraseBuilder.charAt(h) == ' ' || phraseBuilder.charAt(h) == '-' || isPunctuated(h, phraseBuilder.toString())) {
				numberOfSeparator++;
			}
			
		}
		return numberOfSeparator;
	}
	
	private String translatePhrase(StringBuilder phraseBuilder, ArrayList<Character> splitterType, int numberOfSeparator, ArrayList<String> containerOfWords) {
		int z=0;
		int y=0;
		int i=0;
		int separatorCounter=0;
		StringBuilder phraseTransleted = new StringBuilder("");
		for (i=0; i<phraseBuilder.length(); i++) {
			 
			if(phraseBuilder.charAt(i) == splitterType.get(y) || separatorCounter==numberOfSeparator ){
				
				
				if(isLetterVowel( i-1, phraseBuilder.toString()) ||isLetterConsonant( i-1, phraseBuilder.toString())||separatorCounter==numberOfSeparator) {
					
					phraseTransleted.append(containerOfWords.get(z));
					z++;
				}
				if(separatorCounter!=numberOfSeparator) {
				phraseTransleted.append(splitterType.get(y));
				}
				if(y != splitterType.size()-1) {
					y++;
				}
				
				separatorCounter++;
				
			}
		}
		return phraseTransleted.toString();
	}
	
	private ArrayList<String> translateWords(ArrayList<String> containerOfWords){
		int i=0;
		int y=0;
		int j=0;
		for(i=0; i<containerOfWords.size(); i++) {
			
			if(startWithVowel(containerOfWords.get(i))){
				
				if (containerOfWords.get(i).endsWith("y")) {
					containerOfWords.set(i, containerOfWords.get(i) + "nay");
					
					
				} else if (endWithVowel(containerOfWords.get(i))) {
					containerOfWords.set(i, containerOfWords.get(i) + "yay");
					
				}else if (endWithConsonant(containerOfWords.get(i))) {
					containerOfWords.set(i, containerOfWords.get(i) + "ay");
				}
			}else if(startWithConsonant( containerOfWords.get(i))){
					StringBuilder phraseSwapped = new StringBuilder (containerOfWords.get(i));
					int numberOfConsonant=0;
					char temp;
					 y=0;
					 
					while(isLetterConsonant(y,phraseSwapped.toString() )) {
						
						numberOfConsonant++;
						y++;
					}
					
					for(j=0; j<numberOfConsonant; j++) {
						temp = phraseSwapped.charAt(0);
						phraseSwapped.deleteCharAt(0);
						phraseSwapped.append(temp);
					}
					containerOfWords.set(i, phraseSwapped.append("ay").toString());
			}
			
		}
		
		return containerOfWords;
	}
	
	private ArrayList<Character> initializeSplitterType(StringBuilder phraseBuilder, ArrayList<Character> splitterType, int k){
		if(phraseBuilder.charAt(k) == ' ') {
			splitterType.add(' ');
		}else if(phraseBuilder.charAt(k) == '-') {
			splitterType.add('-');
		}else {
			splitterType.add(phraseBuilder.charAt(k));
		}
		
		return splitterType;
	}

}

package piglatin1;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world" , translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translateSingleWord());
	}
	
	@Test
	public void testTranslationStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translateSingleWord());
	}
	
	@Test
	public void testTranslationStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translateSingleWord());
	}
	
	@Test
	public void testTranslationStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translateSingleWord());
	}
	
	@Test
	public void testTranslationStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translateSingleWord());
	}
	
	@Test
	public void testTranslationStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translateSingleWord());
	}

	@Test
	public void testTranslationStartingWithMoreConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translateSingleWord());
	}

	@Test
	public void testTranslationWithMoreWordsSpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translateWordsPunctuated());
	}
	
	@Test
	public void testTranslationWithMoreWordsDash() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translateWordsPunctuated());
	}
	
	@Test
	public void testTranslationWithMoreWordsDashSpace() {
		String inputPhrase = "well-being hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay ellohay orldway", translator.translateWordsPunctuated());
	}
	
	@Test
	public void testTranslationSingleWordWithPunctuation() {
		String inputPhrase = "hello!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay!", translator.translateWordsPunctuated());
	}
	
	@Test
	public void testTranslationMoreWordWithPunctuation() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translateWordsPunctuated());
	}
	
	
}
